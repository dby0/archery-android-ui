package uk.co.divisiblebyzero.archery.domain;

/**
 * Created by Matthew on 24/09/2016.
 */
public enum BowType {
    RECURVE, COMPOUND, BAREBOW, LONGBOW;
}
