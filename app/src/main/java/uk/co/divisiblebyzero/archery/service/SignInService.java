package uk.co.divisiblebyzero.archery.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by Matthew Smalley on 04/09/2016.
 */
public class SignInService extends IntentService {
    private static final String EXTRA_SIGNIN_CODE = "uk.co.divisiblebyzero.archery.action.SIGNIN_CODE";
    private static final String ACTION_SIGNIN = "uk.co.divisiblebyzero.archery.action.SIGNIN";
    private static final String TAG = SignInService.class.getName();
    private static final String LOGIN_URL = NetworkHelper.BASE_URL + "/login/google";

    public SignInService() {
        super("SignInService");
    }


    public static void startSignIn(Context context, String code) {
        Intent intent = new Intent(context, SignInService.class);
        intent.setAction(ACTION_SIGNIN);
        intent.putExtra(EXTRA_SIGNIN_CODE, code);
        context.startService(intent);
    }
    private String getStateFromResponse(HttpResponse response) {
        String state = null;
        for (Header header: response.getAllHeaders()) {
            Log.d(TAG, "Response Header: [" + header.getName() + "]= " + header.getValue());
            if ("Location".equals(header.getName())) {
                String location = header.getValue();
                if (location != null && location.contains("state=")) {
                    state = location.substring(location.indexOf("state=") + 6);
                    if (state.contains("?")) {
                        state = state.substring(0, state.indexOf("?"));
                    }
                }
            }
            Log.d(TAG, "State: " + state);
        }
        return state;
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "Handing Intent...");
        String code = intent.getStringExtra(EXTRA_SIGNIN_CODE);
        Log.i(TAG, "Code is: " + code);
        String state = null;

        try {
            HttpResponse getResponse = NetworkHelper.executeGet(LOGIN_URL, null);
            state = getStateFromResponse(getResponse);
            EntityUtils.consume(getResponse.getEntity());
        } catch (ClientProtocolException e) {
            Log.e("ArcherySignin", "Error sending ID token to backend.", e);
        } catch (IOException e) {
            Log.e("ArcherySignin", "Error sending ID token to backend.", e);
        }


        String urlParams = "code=" + code + "&state=" + state;

        try {
            HttpResponse response = NetworkHelper.executeGet(LOGIN_URL, urlParams);
            Log.d(TAG, "Setting token to: " + code);
            int statusCode = response.getStatusLine().getStatusCode();
            Log.d(TAG, "StatusCode: " + statusCode);
            Log.d(TAG, "Response: " + response);
            final String responseBody = EntityUtils.toString(response.getEntity());

            DataIntentService.startDownload(this);
        } catch (ClientProtocolException e) {
            Log.e(TAG, "Error sending ID token to backend.", e);
        } catch (IOException e) {
            Log.e(TAG, "Error sending ID token to backend.", e);
        }
    }
}
