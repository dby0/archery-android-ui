package uk.co.divisiblebyzero.archery.db;

import android.provider.BaseColumns;

/**
 * Created by Matthew Smalley on 18/09/2016.
 */
public final class ArcheryContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private ArcheryContract() {}

    /**
     *     private int distance;
     private double sightMark;
     private SightMarkUnits units;
     */

    /* Inner class that defines the table contents */
    public static class SightMark implements BaseColumns {
        public static final String TABLE_NAME = "sightmarks";
        public static final String COLUMN_NAME_SERVER_ID = "serverId";
        public static final String COLUMN_NAME_USER_ID = "userId";
        public static final String COLUMN_NAME_DISTANCE = "distance";
        public static final String COLUMN_NAME_SIGHTMARK = "sightMark";
        public static final String COLUMN_NAME_UNITS = "units";
    }
}