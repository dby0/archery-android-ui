package uk.co.divisiblebyzero.archery.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.util.EntityUtils;
import uk.co.divisiblebyzero.archery.domain.ArcheryRound;

/**
 * Created by Matthew Smalley on 01/09/2016.
 */
public class ScoresDataService extends IntentService {
    private static final String SIGHTMARKS_URL = NetworkHelper.BASE_URL + "/archeryRounds";
    private static final String ACTION_DOWNLOAD_SCORES = "uk.co.divisiblebyzero.archery.action.DOWNLOAD_SCORES";

    public static final ScoresDataService INSTANCE = new ScoresDataService();
    private static final String TAG = ScoresDataService.class.toString();
    private static final ObjectMapper MAPPER = new ObjectMapper();

    private Context context;

    private ArrayList<ArcheryRound> archeryRounds = new ArrayList<>();

    private ScoresDataService() {
        super("ScoresDataService");
    }

    private void saveContext(Context context) {
        if (this.context == null) {
            this.context = context;
        }
    }

    private static final void L(String log) {
        Log.i(TAG, log);
    }

    public ArrayList<ArcheryRound> getScores() {

        return archeryRounds;
    }

    public ArcheryRound getArcheryRound(String archeryRoundId) {
        return archeryRounds.get(0);
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class DownloadStructure {
        EmbeddedStructure _embedded;
    }

    public static class EmbeddedStructure {
        ArrayList<ArcheryRound> archeryRounds;
    }

    private void downloadScores() {
        L("Synchronising Scores data with server");

        try {
            HttpResponse response = NetworkHelper.executeGet(SIGHTMARKS_URL, null);
            String data = EntityUtils.toString(response.getEntity());
            DownloadStructure structure = MAPPER.readValue(data, DownloadStructure.class);
            ArrayList<ArcheryRound> archeryRounds = structure._embedded.archeryRounds;
            Collections.sort(archeryRounds, new Comparator<ArcheryRound>() {
                @Override
                public int compare(ArcheryRound archeryRound, ArcheryRound t1) {
                    return archeryRound.getDate().compareTo(t1.getDate());
                }
            });
            deleteAllSightMarks(context);
            for (ArcheryRound archeryRound: archeryRounds) {
                addScoreToDatabase(context, archeryRound);
            }

        } catch (IOException e) {
            e.printStackTrace();
            L("Error retrieving data from server: " + e.getMessage());
        }

    }

    public static void startDownloadScores(Context context) {
        startAction(context, ACTION_DOWNLOAD_SCORES);
    }

    private static void startAction(Context context, String action) {
        Log.i(TAG, "Firing service: " + action);
        Intent intent = new Intent(context, ScoresDataService.class);
        intent.setAction(action);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "Got intent: " + intent.getAction());
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_DOWNLOAD_SCORES.equals(action)) {
                downloadScores();
            }
        }
    }

    private void addScoreToDatabase(Context context, ArcheryRound archeryRound) {
        archeryRounds.add(archeryRound);
    }

    private void deleteAllSightMarks(Context context) {
        archeryRounds = new ArrayList<>();
    }
}
