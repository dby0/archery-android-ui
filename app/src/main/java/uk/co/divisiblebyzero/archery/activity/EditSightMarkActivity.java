package uk.co.divisiblebyzero.archery.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import uk.co.divisiblebyzero.archery.R;
import uk.co.divisiblebyzero.archery.domain.SightMark;
import uk.co.divisiblebyzero.archery.domain.SightMarkUnits;
import uk.co.divisiblebyzero.archery.service.SightMarkDataService;

public class EditSightMarkActivity extends AppCompatActivity {
    private static final String TAG = EditSightMarkActivity.class.getName();
    private SightMarkUnits units;
    public static final String EXTRA_SIGHT_MARK_LOCAL_ID = "SIGHT_MARK_LOCAL_ID";
    private SightMarkDataService sightMarkDataService;
    private SightMark sightMark;
    private long sightMarkLocalId = -1;
    private boolean mBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sight_mark);
        Intent intent = getIntent();
        sightMarkLocalId = intent.getLongExtra(EXTRA_SIGHT_MARK_LOCAL_ID, -1);


        Intent sightMarkDataServiceIntent = new Intent(this, SightMarkDataService.class);
        bindService(sightMarkDataServiceIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private void resetView() {
        if (sightMarkLocalId != -1) {
            this.sightMark = sightMarkDataService.getSightMark(sightMarkLocalId);
            if (sightMark != null) {
                if (SightMarkUnits.METRES == sightMark.getUnits()) {
                    RadioButton unitButton = (RadioButton)findViewById(R.id.addSightMarkRadioButtonUnitsMetres);
                    unitButton.setChecked(true);
                    units = SightMarkUnits.METRES;
                } else if (SightMarkUnits.YARDS == sightMark.getUnits()) {
                    RadioButton unitButton = (RadioButton)findViewById(R.id.addSightMarkRadioButtonUnitsYards);
                    unitButton.setChecked(true);
                    units = SightMarkUnits.YARDS;
                }
            }
            EditText distance = (EditText)findViewById(R.id.addSightMarkDistance);
            distance.setText(""+sightMark.getDistance());
            EditText sightMarkEditText = (EditText)findViewById(R.id.addSightMarkEditTextSightMark);
            sightMarkEditText.setText(""+sightMark.getSightMark());
            TextView title = (TextView)findViewById(R.id.addSightMarkTitle);
            title.setText("Edit Sight Mark");
        }
    }

    public void onRadioButtonClick(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.addSightMarkRadioButtonUnitsMetres:
                if (checked) {
                    units = SightMarkUnits.METRES;
                }
                break;
            case R.id.addSightMarkRadioButtonUnitsYards:
                if (checked) {
                    units = SightMarkUnits.YARDS;
                }
                break;
        }
    }

    public void onDoneClick(View view) {
        EditText distance = (EditText) findViewById(R.id.addSightMarkDistance);
        EditText sightMarkText = (EditText) findViewById(R.id.addSightMarkEditTextSightMark);
        if (distance.getText() != null) {
            if (sightMark == null) {
                sightMark = new SightMark();
            }
            String distanceString = distance.getText().toString();
            if (distanceString != null && !"".equals(distanceString)) {
                sightMark.setDistance(Integer.valueOf(distanceString));
            }
            sightMark.setUnits(units);
            String sightMarkString = sightMarkText.getText().toString();
            if (sightMarkString != null && !"".equals(sightMarkString)) {
                sightMark.setSightMark(Double.valueOf(sightMarkString));
            }
            sightMarkDataService.updateSightMark(sightMark);
        }
        Intent intent = new Intent(this, SightMarksActivity.class);
        startActivity(intent);
    }

    public void onDeleteClick(View view) {
        if (sightMark != null) {
            sightMarkDataService.deleteSightMark(sightMark);
        }

        Intent intent = new Intent(this, SightMarksActivity.class);
        startActivity(intent);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            SightMarkDataService.SightMarkBinder binder = (SightMarkDataService.SightMarkBinder) service;
            sightMarkDataService = binder.getService();
            mBound = true;
            resetView();

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


    private final void bind() {
        Log.i(TAG, "Creating intent");
        Intent intent = new Intent(getApplicationContext(), SightMarkDataService.class);
        Log.i(TAG, "Binding");
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        Log.i(TAG, "Leaving onCreate");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mBound) {
            bind();
        } else {
            resetView();
        }
    }
}
