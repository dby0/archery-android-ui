package uk.co.divisiblebyzero.archery.domain;

import java.util.List;

/**
 * Created by Matthew on 24/09/2016.
 */
public class End {
    private List<Integer> scores;
    private int endTotal;

    public List<Integer> getScores() {
        return scores;
    }

    public void setScores(List<Integer> scores) {
        this.scores = scores;
    }

    public int getEndTotal() {
        return endTotal;
    }

    public void setEndTotal(int endTotal) {
        this.endTotal = endTotal;
    }
}
