package uk.co.divisiblebyzero.archery.domain;

import java.util.Set;

/**
 * Created by Matthew Smalley on 04/09/2016.
 */
public class User {
    private String name;

    private String email;
    private Set<String> roles;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", roles=" + roles +
                '}';
    }
}
