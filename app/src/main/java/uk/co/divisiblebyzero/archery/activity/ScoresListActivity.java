package uk.co.divisiblebyzero.archery.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import uk.co.divisiblebyzero.archery.R;
import uk.co.divisiblebyzero.archery.domain.ArcheryRound;
import uk.co.divisiblebyzero.archery.domain.SightMark;
import uk.co.divisiblebyzero.archery.service.ScoresDataService;

public class ScoresListActivity extends AppCompatActivity {
    private static final String TAG = ScoresListActivity.class.getName();
    private ScoresDataService scoresDataService = ScoresDataService.INSTANCE;
    private ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores_list);

        lv = (ListView) findViewById(R.id.scores_list_view);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {
                ArcheryRound archeryRound = (ArcheryRound)adapter.getItemAtPosition(position);
                Log.i(TAG, "Clicked on: " + archeryRound.getServerId());
                launchViewer(archeryRound);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "Downloading scores");
        ArrayList<ArcheryRound> archeryRounds = scoresDataService.getScores();

        ArrayAdapter<ArcheryRound> arrayAdapter = new ArcheryRoundArrayAdapter(
                this,
                archeryRounds );

        lv.setAdapter(arrayAdapter);
    }

    private void launchViewer(ArcheryRound archeryRound) {
        Log.i(TAG, "Launching view activity");
        Intent intent = new Intent(this, ViewScoreActivity.class);
        intent.putExtra(ViewScoreActivity.EXTRA_ARCHERY_ROUND_ID, archeryRound.getServerId());
        startActivity(intent);
    }
}
