package uk.co.divisiblebyzero.archery.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import uk.co.divisiblebyzero.archery.R;
import uk.co.divisiblebyzero.archery.domain.SightMark;

/**
 * Created by Matthew Smalley on 29/08/2016.
 */
public class SightMarkArrayAdapter extends ArrayAdapter<SightMark> {
    private final Context context;
    private final ArrayList<SightMark> values;

    public SightMarkArrayAdapter(Context context, ArrayList<SightMark> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.view_sight_mark, parent, false);
        TextView distance = (TextView) rowView.findViewById(R.id.distance);
        TextView sightMark = (TextView) rowView.findViewById(R.id.sightMark);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        distance.setText(values.get(position).getDistance() + " " + values.get(position).getUnits());
        sightMark.setText(Double.valueOf(values.get(position).getSightMark()).toString());
        return rowView;
    }
}
