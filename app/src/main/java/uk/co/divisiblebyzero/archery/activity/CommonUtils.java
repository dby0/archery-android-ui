package uk.co.divisiblebyzero.archery.activity;

import android.app.Activity;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Matthew on 25/09/2016.
 */

public class CommonUtils {
    private static final String PATTERN = "dd/MM/yyyy";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(PATTERN);

    public static void wireCheckBox(Activity activity, int element, boolean value) {
        CheckBox checkBox = (CheckBox)activity.findViewById(element);
        checkBox.setChecked(value);
    }

    public static void wireTextView(Activity activity, int element, String text) {
        TextView textView = (TextView) activity.findViewById(element);
        textView.setText(text);
    }

    public static void wireTextView(Activity activity, int element, int value) {
        String text = Integer.toString(value);
        wireTextView(activity, element, text);
    }

    public static void wireTextView(Activity activity, int element, Date date) {
        String text = convert(date);
        wireTextView(activity, element, text);
    }

    public static String convert(Date date) {
        return DATE_FORMAT.format(date);
    }
}
