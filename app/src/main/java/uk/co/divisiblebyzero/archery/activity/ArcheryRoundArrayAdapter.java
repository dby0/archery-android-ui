package uk.co.divisiblebyzero.archery.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import uk.co.divisiblebyzero.archery.R;
import uk.co.divisiblebyzero.archery.domain.ArcheryRound;
import uk.co.divisiblebyzero.archery.domain.SightMark;

/**
 * Created by Matthew Smalley on 29/08/2016.
 */
public class ArcheryRoundArrayAdapter extends ArrayAdapter<ArcheryRound> {
    private final Context context;
    private final ArrayList<ArcheryRound> values;

    public ArcheryRoundArrayAdapter(Context context, ArrayList<ArcheryRound> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.view_score, parent, false);
        ArcheryRound round = values.get(position);
        TextView title = (TextView) rowView.findViewById(R.id.score_title);
        TextView details = (TextView) rowView.findViewById(R.id.score_details);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        title.setText(round.getArcher() + " " + round.getDate());
        details.setText("Score: " + round.getTotalScore() + ", implied handicap: " + round.getHandicap());
        return rowView;
    }
}
