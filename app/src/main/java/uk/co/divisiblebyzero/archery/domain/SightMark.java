package uk.co.divisiblebyzero.archery.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Matthew Smalley on 29/08/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SightMark {
    private long localId;
    @JsonProperty("id")
    private String serverId;
    private String userId;
    private int distance;
    private double sightMark;
    private SightMarkUnits units;
    private long timestamp;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getLocalId() {
        return localId;
    }

    public void setLocalId(long localId) {
        this.localId = localId;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public double getSightMark() {
        return sightMark;
    }

    public void setSightMark(double sightMark) {
        this.sightMark = sightMark;
    }

    public SightMarkUnits getUnits() {
        return units;
    }

    public void setUnits(SightMarkUnits units) {
        this.units = units;
    }

    @Override
    public String toString() {
        return "SightMark{" +
                "units=" + units +
                ", sightMark=" + sightMark +
                ", distance=" + distance +
                ", userId='" + userId + '\'' +
                ", serverId='" + serverId + '\'' +
                ", localId=" + localId +
                '}';
    }
}
