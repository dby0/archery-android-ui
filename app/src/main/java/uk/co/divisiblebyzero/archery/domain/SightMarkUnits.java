package uk.co.divisiblebyzero.archery.domain;

/**
 * Created by Matthew Smalley on 29/08/2016.
 */
public enum SightMarkUnits {
    YARDS, METRES
}
