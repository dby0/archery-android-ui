package uk.co.divisiblebyzero.archery.service;

import android.util.Log;

import java.io.IOException;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.CookieStore;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.client.protocol.ClientContext;
import cz.msebera.android.httpclient.cookie.Cookie;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.BasicCookieStore;
import cz.msebera.android.httpclient.impl.client.CloseableHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.protocol.BasicHttpContext;
import cz.msebera.android.httpclient.protocol.HttpContext;

/**
 * Created by Matthew Smalley on 01/09/2016.
 */
public class NetworkHelper {
    private static final String TAG = NetworkHelper.class.getName();
    public static final String BASE_URL = "https://archerydiy-dby0.rhcloud.com";
    //public static final String BASE_URL = "http://couscous:8080/";

    private static HttpClientBuilder clientBuilder;
    private static CloseableHttpClient httpClient;
    private static HttpContext context;
    private static CookieStore cookieStore;
    private static boolean SIGNED_IN = false;
    private static String xsrfToken;

    public static boolean getSignedIn() {
        return SIGNED_IN;
    }

    public static void setSignedIn(boolean signedIn) {
        SIGNED_IN = signedIn;
    }

    public static synchronized HttpClient getHttpClient() {
        if (httpClient == null) {
            clientBuilder = HttpClientBuilder.create();
            httpClient = clientBuilder.disableRedirectHandling().build();
            cookieStore = new BasicCookieStore();
            context = new BasicHttpContext();
            context.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
        }
        return httpClient;
    }

    public static synchronized HttpContext getHttpContext() {
        return context;
    }

    public static CookieStore getCookieStore() {
        return cookieStore;
    }

    private static String buildUrl(String url, String urlParams) {
        String builtUrl = url;
        String xsrfToken = getXsrfToken();
        if (xsrfToken != null) {
            builtUrl = url + "?_csrf=" + getXsrfToken();
            if (urlParams != null) {
                builtUrl += "&" + urlParams;
            }
        } else {
            if (urlParams != null) {
                builtUrl += "?" + urlParams;
            }
        }
        return builtUrl;
    }

    public static HttpResponse executeGet(String url, String urlParams) throws IOException {
        String builtUrl = buildUrl(url, urlParams);
        Log.d(TAG, "GET Requesting: " + builtUrl);
        HttpGet get = new HttpGet(builtUrl);
        return getHttpClient().execute(get, context);
    }

    public static HttpResponse executePost(String url, String urlParams, String payload) throws IOException {
        String builtUrl = buildUrl(url, urlParams);
        Log.d(TAG, "POST Requesting: " + builtUrl);
        HttpPost post = new HttpPost(builtUrl);
        post.setEntity(new StringEntity(payload));
        return getHttpClient().execute(post, context);
    }

    public static HttpResponse executeJsonPost(String url, String urlParams, String payload) throws IOException {
        String builtUrl = buildUrl(url, urlParams);
        Log.d(TAG, "POST Requesting: " + builtUrl);
        HttpPost post = new HttpPost(builtUrl);
        post.setHeader("Accept", "application/json");
        post.setHeader("Content-type", "application/json");
        post.setEntity(new StringEntity(payload));
        return getHttpClient().execute(post, context);
    }

    private static String getXsrfToken() {
        if (xsrfToken == null) {
            if (getCookieStore() != null) {
                List<Cookie> cookies = getCookieStore().getCookies();
                Log.d(TAG, "Cookies: " + cookies);
                for (Cookie cookie : cookies) {
                    if ("XSRF-TOKEN".equals(cookie.getName())) {
                        Log.d("ArcherySignIn", "XSRF-TOKEN=" + cookie.getValue());
                        xsrfToken = cookie.getValue();
                    }
                }
            }
        }
        return xsrfToken;
    }
}
