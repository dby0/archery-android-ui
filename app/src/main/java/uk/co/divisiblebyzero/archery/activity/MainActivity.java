package uk.co.divisiblebyzero.archery.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import uk.co.divisiblebyzero.archery.R;
import uk.co.divisiblebyzero.archery.service.DataIntentService;
import uk.co.divisiblebyzero.archery.service.SightMarkDataService;
import uk.co.divisiblebyzero.archery.service.SignInService;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = MainActivity.class.getName();
    private DataIntentService mService;
    private static final int RC_SIGN_IN = 9900;
    private boolean mBound = false;
    private TextView mStatusTextView;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent sightMarkDataServiceIntent = new Intent(this, SightMarkDataService.class);
        startService(sightMarkDataServiceIntent);
        Intent dataIntentServiceIntent = new Intent(this, DataIntentService.class);
        startService(dataIntentServiceIntent);
        bindService(dataIntentServiceIntent, mConnection, Context.BIND_AUTO_CREATE);

        mStatusTextView = (TextView) findViewById(R.id.textViewSignInStatus);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                //This should be the client id for the server/service we're accessing.
                .requestServerAuthCode("607720498390-prkfpk9mvuckf88abdvork7pteqe03og.apps.googleusercontent.com")
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        signIn();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onSightMarksClick(View view) {
        Intent intent = new Intent(this, SightMarksActivity.class);
        startActivity(intent);
    }

    public void onScoresClick(View view) {
        Intent intent = new Intent(this, ScoresListActivity.class);
        startActivity(intent);
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            DataIntentService.DataBinder binder = (DataIntentService.DataBinder) service;
            mService = binder.getService();
            mBound = true;

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.i(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            mStatusTextView.setText(acct.getDisplayName());

            String idToken = acct.getServerAuthCode();
            Log.i("ArcherySignIn", "ID Token is: " + idToken);

            SignInService.startSignIn(this, idToken);
        } else {

            mStatusTextView.setText(result.getStatus().toString());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "onConnectionFailed:" + connectionResult);
    }
}
