package uk.co.divisiblebyzero.archery.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.util.EntityUtils;
import uk.co.divisiblebyzero.archery.db.ArcheryContract;
import uk.co.divisiblebyzero.archery.db.ArcheryDbHelper;
import uk.co.divisiblebyzero.archery.domain.SightMark;
import uk.co.divisiblebyzero.archery.domain.SightMarkUnits;

/**
 * Created by Matthew Smalley on 01/09/2016.
 */
public class SightMarkDataService extends IntentService {
    private static final String TAG = SightMarkDataService.class.getName();
    private static final String ACTION_DOWNLOAD_SIGHTMARKS = "uk.co.divisiblebyzero.archery.action.DOWNLOAD_SIGHTMARKS";
    private static final String SIGHTMARKS_URL = NetworkHelper.BASE_URL + "/sightMarks";

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final void L(String log) {
        Log.i(TAG, log);
    }

    public SightMarkDataService() {
        super("SightMarkDataService");
    }

    private SightMark createFromCursor(Cursor c) {
        SightMark sightMark = new SightMark();
        sightMark.setLocalId(c.getLong(c.getColumnIndexOrThrow(ArcheryContract.SightMark._ID)));
        sightMark.setServerId(c.getString(c.getColumnIndexOrThrow(ArcheryContract.SightMark.COLUMN_NAME_SERVER_ID)));
        sightMark.setUserId(c.getString(c.getColumnIndexOrThrow(ArcheryContract.SightMark.COLUMN_NAME_USER_ID)));
        sightMark.setDistance(c.getInt(c.getColumnIndexOrThrow(ArcheryContract.SightMark.COLUMN_NAME_DISTANCE)));
        sightMark.setSightMark(c.getDouble(c.getColumnIndexOrThrow(ArcheryContract.SightMark.COLUMN_NAME_SIGHTMARK)));
        sightMark.setUnits(SightMarkUnits.valueOf(c.getString(c.getColumnIndexOrThrow(ArcheryContract.SightMark.COLUMN_NAME_UNITS))));
        return sightMark;
    }

    private Cursor getCursor(Context context, long id) {
        SQLiteDatabase db = new ArcheryDbHelper(context).getReadableDatabase();

        String[] projection = {
                ArcheryContract.SightMark._ID,
                ArcheryContract.SightMark.COLUMN_NAME_SERVER_ID,
                ArcheryContract.SightMark.COLUMN_NAME_USER_ID,
                ArcheryContract.SightMark.COLUMN_NAME_DISTANCE,
                ArcheryContract.SightMark.COLUMN_NAME_UNITS,
                ArcheryContract.SightMark.COLUMN_NAME_SIGHTMARK
        };

        String selection = null;
        String[] selectionArgs = null;

        if (id != 0) {
            selection = ArcheryContract.SightMark._ID + " = ?";
            selectionArgs = new String[] { "" + id };

        }

        Cursor c = db.query(
                ArcheryContract.SightMark.TABLE_NAME,     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null // The sort order
        );

        return c;
    }

    public ArrayList<SightMark> getSightMarks() {

        L("Retrieving sight marks");


        ArrayList<SightMark> sightMarks = new ArrayList<>();

        Cursor c = getCursor(this, 0);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            sightMarks.add(createFromCursor(c));
            c.moveToNext();
        }
        c.close();
        L("Returning sight marks array: " + sightMarks);
        return sightMarks;
    }

    public void updateSightMark(SightMark sightMark) {
        L("Updating sightmark: " + sightMark);
        if (sightMark.getLocalId() == 0) {
            addSightMarkToDatabase(sightMark);
            saveSightMarkToServer(sightMark);
        } else {
            updateSightMarkInDatabase(sightMark);
            saveSightMarkToServer(sightMark);
        }
    }

    private void updateSightMarkInDatabase(SightMark sightMark) {
        L("Updating database for: " + sightMark);
        if (0 == sightMark.getLocalId()) {
            L("Don't think this sight mark ever made it to the db... skipping");
            return;
        }
        SQLiteDatabase db = new ArcheryDbHelper(this).getReadableDatabase();


        // Which row to update, based on the title
        String selection = ArcheryContract.SightMark._ID + " = ?";
        String[] selectionArgs = { ""+sightMark.getLocalId() };

        int count = db.update(
                ArcheryContract.SightMark.TABLE_NAME,
                getValues(sightMark),
                selection,
                selectionArgs);
        L("Updated: " + count + " rows.");
    }

    private ContentValues getValues(SightMark sightMark) {
        ContentValues values = new ContentValues();
        values.put(ArcheryContract.SightMark.COLUMN_NAME_SERVER_ID, sightMark.getServerId());
        values.put(ArcheryContract.SightMark.COLUMN_NAME_USER_ID, sightMark.getUserId());
        values.put(ArcheryContract.SightMark.COLUMN_NAME_DISTANCE, sightMark.getDistance());
        values.put(ArcheryContract.SightMark.COLUMN_NAME_SIGHTMARK, sightMark.getSightMark());
        values.put(ArcheryContract.SightMark.COLUMN_NAME_UNITS, sightMark.getUnits().toString());
        return values;
    }


    private void addSightMarkToDatabase(SightMark sightMark) {
        L("Saving sight mark to database");
        SQLiteDatabase db = new ArcheryDbHelper(this).getReadableDatabase();

        long newId = db.insert(ArcheryContract.SightMark.TABLE_NAME, null, getValues(sightMark));
        L("Done, new id is: " + newId);
    }

    public static void startDownloadSightMarks(Context context) {
        startAction(context, ACTION_DOWNLOAD_SIGHTMARKS);
    }

    private static void startAction(Context context, String action) {
        Log.i(TAG, "Firing service: " + action);
        Intent intent = new Intent(context, SightMarkDataService.class);
        intent.setAction(action);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "Got intent: " + intent.getAction());
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_DOWNLOAD_SIGHTMARKS.equals(action)) {
                downloadSightMarks();
            }
        }
    }

    private final static class SaveSightMarkAsyncTask extends AsyncTask<String, Integer, Long> {
        @Override
        protected Long doInBackground(String... params) {
            L("Sending to server");
            try {
                for (String sightMarkJson: params) {
                    L("Sending this to server: " + sightMarkJson);
                    HttpResponse response = NetworkHelper.executeJsonPost(SIGHTMARKS_URL, null, sightMarkJson);
                    String returnJson = EntityUtils.toString(response.getEntity());
                    L("Got this back: " + returnJson);
                    SightMark newSightMark = MAPPER.readValue(returnJson, SightMark.class);
                }

            } catch (IOException e) {
                e.printStackTrace();
                L("Error communicating with server: " + e.getMessage());
            }
            return null;
        }
    }

    private void saveSightMarkToServer(final SightMark sightMark) {
        L("Saving sight mark to server");

        try {
            final String sightMarkJson = MAPPER.writeValueAsString(sightMark);
            new SaveSightMarkAsyncTask().execute(sightMarkJson);
        } catch (Exception e) {
            e.printStackTrace();
            L("Error creating JSON for sight mark: " + e.getMessage());
        }
    }

    public SightMark getSightMark(Long localId) {
        L("Getting sight mark: " + localId);
        SightMark sightMark = null;
        Cursor c = getCursor(this, localId);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            sightMark = createFromCursor(c);
            c.moveToNext();
        }
        c.close();
        return sightMark;
    }

    public void deleteSightMark(SightMark sightMark) {
        L("Deleting sight mark: " + sightMark);
        if (0 == sightMark.getLocalId()) {
            L("Don't think this sight mark ever made it to the db... skipping");
            return;
        }
        SQLiteDatabase db = new ArcheryDbHelper(this).getReadableDatabase();
        // Which row to update, based on the title
        String selection = ArcheryContract.SightMark._ID + " = ?";
        String[] selectionArgs = { ""+sightMark.getLocalId() };

        int count = db.delete(ArcheryContract.SightMark.TABLE_NAME, selection, selectionArgs);
        L("Deleted: " + count + " rows");
    }

    public void deleteAllSightMarks() {
        L("Deleting all sight marks!");
        SQLiteDatabase db = new ArcheryDbHelper(this).getReadableDatabase();

        int count = db.delete(ArcheryContract.SightMark.TABLE_NAME, null, null);
        L("Deleted: " + count + " rows");
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class DownloadStructure {
        EmbeddedStructure _embedded;
    }

    public static class EmbeddedStructure {
        ArrayList<SightMark> sightMarks;
    }

    private void downloadSightMarks() {


        L("Synchronising Sight Marks data with server");

        try {
            HttpResponse response = NetworkHelper.executeGet(SIGHTMARKS_URL, null);
            String data = EntityUtils.toString(response.getEntity());
            DownloadStructure structure = MAPPER.readValue(data, DownloadStructure.class);
            ArrayList<SightMark> sightMarks = structure._embedded.sightMarks;
            deleteAllSightMarks();
            for (SightMark sightMark: sightMarks) {
                addSightMarkToDatabase(sightMark);
            }

        } catch (IOException e) {
            e.printStackTrace();
            L("Error retrieving data from server: " + e.getMessage());
        }
    }

    private final IBinder mBinder = new SightMarkDataService.SightMarkBinder();

    public class SightMarkBinder extends Binder {
        public SightMarkDataService getService() {
            return SightMarkDataService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
}
