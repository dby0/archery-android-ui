package uk.co.divisiblebyzero.archery.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import uk.co.divisiblebyzero.archery.R;
import uk.co.divisiblebyzero.archery.domain.SightMark;
import uk.co.divisiblebyzero.archery.service.SightMarkDataService;

public class SightMarksActivity extends AppCompatActivity {
    private static final String TAG = SightMarksActivity.class.getName();
    private SightMarkDataService sightMarkDataService;
    private ListView lv;

    private boolean mBound;

    private void launchEditor(SightMark sm) {
        Intent intent = new Intent(this, EditSightMarkActivity.class);
        intent.putExtra(EditSightMarkActivity.EXTRA_SIGHT_MARK_LOCAL_ID, sm.getLocalId());
        startActivity(intent);
    }

    public void onAddClick(View view) {
        Intent intent = new Intent(this, EditSightMarkActivity.class);
        startActivity(intent);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            Log.i(TAG, "Got binder");
            SightMarkDataService.SightMarkBinder binder = (SightMarkDataService.SightMarkBinder) service;
            sightMarkDataService = binder.getService();
            mBound = true;
            resetView();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    private final void resetView() {
        if (mBound) {
            ArrayAdapter<SightMark> arrayAdapter = new SightMarkArrayAdapter(
                    this,
                    sightMarkDataService.getSightMarks() );
            lv.setAdapter(arrayAdapter);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sight_marks);
        lv = (ListView) findViewById(R.id.listView);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {
                SightMark sm = (SightMark)adapter.getItemAtPosition(position);
                Log.i("Archery", "Clicked on: " + sm.getDistance());
                launchEditor(sm);
            }
        });
    }

    private final void bind() {
        Log.i(TAG, "Creating intent");
        Intent intent = new Intent(getApplicationContext(), SightMarkDataService.class);
        Log.i(TAG, "Binding");
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        Log.i(TAG, "Leaving onCreate");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mBound) {
            bind();
        } else {
            resetView();
        }
    }
}
