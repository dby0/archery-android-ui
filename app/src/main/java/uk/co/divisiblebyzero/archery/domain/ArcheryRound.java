package uk.co.divisiblebyzero.archery.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

/**
 * Created by Matthew on 24/09/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArcheryRound {
    private long localId;
    @JsonProperty("id")
    private String serverId;

    private String archer;
    private Date date;
    private String time;
    private BowType bowType;
    private boolean indoor;
    private String roundType;
    private int totalScore;
    private String location;
    private List<Dozen> dozens;
    private int handicap;

    public String getArcher() {
        return archer;
    }

    public void setArcher(String archer) {
        this.archer = archer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BowType getBowType() {
        return bowType;
    }

    public void setBowType(BowType bowType) {
        this.bowType = bowType;
    }

    public boolean isIndoor() {
        return indoor;
    }

    public void setIndoor(boolean indoor) {
        this.indoor = indoor;
    }

    public String getRoundType() {
        return roundType;
    }

    public void setRoundType(String roundType) {
        this.roundType = roundType;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public List<Dozen> getDozens() {
        return dozens;
    }

    public void setDozens(List<Dozen> dozens) {
        this.dozens = dozens;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getHandicap() {
        return handicap;
    }

    public void setHandicap(int handicap) {
        this.handicap = handicap;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }
}
