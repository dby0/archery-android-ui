package uk.co.divisiblebyzero.archery.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import uk.co.divisiblebyzero.archery.R;
import uk.co.divisiblebyzero.archery.domain.ArcheryRound;
import uk.co.divisiblebyzero.archery.service.ScoresDataService;

import static uk.co.divisiblebyzero.archery.activity.CommonUtils.wireCheckBox;
import static uk.co.divisiblebyzero.archery.activity.CommonUtils.wireTextView;

public class ViewScoreActivity extends AppCompatActivity {
    private ScoresDataService scoresDataService = ScoresDataService.INSTANCE;
    public static final String EXTRA_ARCHERY_ROUND_ID = "ARCHERY_ROUND_ID";
    private static final String TAG = ViewScoreActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_score);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        String archeryRoundId = intent.getStringExtra(EXTRA_ARCHERY_ROUND_ID);
        ArcheryRound archeryRound = scoresDataService.getArcheryRound(archeryRoundId);
        wireGui(archeryRound);
    }

    private void wireGui(ArcheryRound archeryRound) {
        wireTextView(this, R.id.viewScoreArcherText, archeryRound.getArcher());
        wireTextView(this, R.id.viewScoreDateText, archeryRound.getDate());
        wireTextView(this, R.id.viewScoreTimeText, archeryRound.getTime());
        wireTextView(this, R.id.viewScoreBowTypeText, archeryRound.getBowType().name());
        wireCheckBox(this, R.id.viewScoreIndoorCheckBox, archeryRound.isIndoor());
        wireTextView(this, R.id.viewScoreRoundTypeText, archeryRound.getRoundType().toString());
        wireTextView(this, R.id.viewScoreTotalScoreText, archeryRound.getTotalScore());
        wireTextView(this, R.id.viewScoreHandicapText, archeryRound.getHandicap());
    }
}
