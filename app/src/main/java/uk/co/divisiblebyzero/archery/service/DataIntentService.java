package uk.co.divisiblebyzero.archery.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class DataIntentService extends IntentService {
    private static final String TAG = DataIntentService.class.getName();
    private static final String ACTION_DOWNLOAD = "uk.co.divisiblebyzero.archery.action.DOWNLOAD";
    private final IBinder mBinder = new DataBinder();
    public DataIntentService() {
        super("DataIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate");
    }

    private static void startAction(Context context, String action) {
        Log.i(TAG, "Firing service: " + action);
        Intent intent = new Intent(context, DataIntentService.class);
        intent.setAction(action);
        context.startService(intent);
    }

    public static void startDownload(Context context) {
        startAction(context, ACTION_DOWNLOAD);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "Got intent: " + intent.getAction());
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_DOWNLOAD.equals(action)) {
                handleActionDownload();
            }
        }
    }

    public void handleActionDownload() {
        Log.i(TAG, "Downloading...");
        Toast.makeText(getApplicationContext(), "Downloading", Toast.LENGTH_LONG).show();
        SightMarkDataService.startDownloadSightMarks(getApplicationContext());
        ScoresDataService.startDownloadScores(getApplicationContext());
    }

    public class DataBinder extends Binder {
        public DataIntentService getService() {
            return DataIntentService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
}
